name := """altimeter"""

version := "1.0"

scalaVersion := "2.11.6"

resolvers += Resolver.jcenterRepo
resolvers +=Resolver.typesafeRepo("releases")

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.4.0",
  "com.typesafe.akka" %% "akka-testkit" % "2.4.0" % "test",
  "org.scalatest" %% "scalatest" % "2.2.5" % "test")
