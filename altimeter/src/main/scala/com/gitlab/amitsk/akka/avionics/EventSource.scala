package com.gitlab.amitsk.akka.avionics

import akka.actor.{Actor, ActorRef}

object EventSource {
  // Messages used by listeners to register and unregister
  // themselves
  case class RegisterListener(listener: ActorRef)
  case class UnregisterListener(listener: ActorRef)
}


trait EventSource {
  def sendEvent[T](event: T): Unit
  def eventSourceReceive: Actor.Receive
}

