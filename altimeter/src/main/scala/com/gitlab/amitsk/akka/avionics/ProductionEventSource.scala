package com.gitlab.amitsk.akka.avionics

import akka.actor.{Actor, ActorRef}

trait ProductionEventSource extends EventSource{  this: Actor =>
  import  EventSource._

  // We're going to use a Vector but many structures would be
  // adequate

  var listeners = Vector.empty[ActorRef]

  //sends event to all listeners
  def sendEvent[T] ( event : T) : Unit = {
    listeners.foreach( _ ! event )
  }

  // We create a specific partial function to handle the
  // messages for our event listener.  Anything that mixes in
  // our trait will need to compose this receiver
  def eventSourceReceive: Receive = {
    case RegisterListener(listener) =>
      listeners = listeners :+ listener
    case UnregisterListener(listener) =>
      listeners = listeners filter { _ != listener }
  }

}

