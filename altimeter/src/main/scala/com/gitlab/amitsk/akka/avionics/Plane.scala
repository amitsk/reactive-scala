package com.gitlab.amitsk.akka.avionics

import akka.actor.Actor.Receive
import akka.actor.{Props, ActorLogging, Actor}
import com.gitlab.amitsk.akka.avionics.EventSource.RegisterListener


object Plane {

  // Returns the control surface to the Actor that
  // asks for them
  case object GiveMeControl

}

// We want the Plane to own the Altimeter and we're going to
// do that by passing in a specific factory we can use to
// build the Altimeter

class Plane extends Actor with ActorLogging {

  import Altimeter._
  import Plane._

  val altimeter = context.actorOf(Altimeter.props(), "Altimeter")
  val controls = context.actorOf(ControlSurfaces.props(altimeter), "ControlSurfaces")

  override def receive: Receive = {
    case AltitudeUpdate(altitude) =>
      log info (s"Altitude is now: $altitude")

    case GiveMeControl =>
      log info ("Plane giving control.")
      sender ! controls
  }

  override def preStart() {
    altimeter ! RegisterListener(self)
  }
}
